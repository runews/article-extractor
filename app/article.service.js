var read = require('node-readability')

module.exports = function() {
    const app = this;
    const getArticle = {
        find(params) {
            return new Promise((resolve) => {
                read(params.query.url, function(err, article, meta) {
                // Main Article
                resolve({content: article.content, title: article.title})
                article.close();
                });
            })
        }
    }

    app.use('/article', getArticle)
}